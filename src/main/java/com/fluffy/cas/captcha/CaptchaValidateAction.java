//package com.fluffy.cas.captcha;
//
//import javax.servlet.http.HttpServletRequest;
//import net.tanesha.recaptcha.ReCaptchaImpl;
//import net.tanesha.recaptcha.ReCaptchaResponse;
//import org.apache.commons.lang.StringUtils;
//import org.jasig.cas.web.support.WebUtils;
//import org.springframework.util.Assert;
//import org.springframework.webflow.action.AbstractAction;
//import org.springframework.webflow.execution.Event;
//import org.springframework.webflow.execution.RequestContext;
// 
//public class CaptchaValidateAction extends AbstractAction {
//    private ReCaptchaImpl reCaptcha = null;
//     
//    public CaptchaValidateAction(ReCaptchaImpl recaptcha) {
//        this.reCaptcha = recaptcha;
//        Assert.notNull(this.reCaptcha, "ReCaptchaImpl required");
//    }
//     
//    @Override
//    protected Event doExecute(RequestContext context) throws Exception {
//        
//        HttpServletRequest request = WebUtils.getHttpServletRequest(context);
//        String recaptchaChallengeField = request.getParameter("recaptcha_challenge_field");
//        String recaptchaResponseField = request.getParameter("recaptcha_response_field");
//        String showCaptchaField = request.getParameter("showCaptcha");
//       
//        try{
//            if ((StringUtils.isNotBlank(showCaptchaField)) &&
//                !verifyCaptcha(request.getRemoteAddr(), recaptchaChallengeField, recaptchaResponseField)) {
//                System.out.println("-----------CaptchaValidateAction.java  , returning error-----------------------");
//                return error();
//            }
//        }
//        catch(Exception e){
//            System.out.println("------------Exception in the if block of CaptchaValidateAction.java-------------");
//            e.printStackTrace();
//            return error();
//        }
//        System.out.println("-----------CaptchaValidateAction.java  , returning success()-----------------------");
//        return success();
//    }
//     
//    /**
//     * Call recaptcha4j to verify captcha response
//     *
//     * @param remoteAddress ip address of the client
//     * @param challenge the recaptcha challenge field
//     * @param response the recaptcha response field
//     * @return boolean indicating verification success or failure
//     */
//    protected boolean verifyCaptcha(String remoteAddress, String challenge, String response) {       
//        System.out.println("-----------inside verifyCaptcha()-------------");
////        System.out.println("");
//        reCaptcha.setRecaptchaServer(ReCaptchaImpl.HTTPS_SERVER);
//        
//        ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddress, challenge, response);
//        
//        return reCaptchaResponse.isValid();
//    }
//}

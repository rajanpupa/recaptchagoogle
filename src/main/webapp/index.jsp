<%@ page import="net.tanesha.recaptcha.ReCaptcha"%>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory"%>

<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<body>
	<h3>Hello World, Welcome the the recaptcha form...</h3>
	<form action="submit2.jsp" method="post">
			<spring:eval expression="@reCaptchaPublicKey" var="publickey" />
			<spring:eval expression="@reCaptchaPrivateKey" var="privatekey" />
			<%
				String publickey= pageContext.getAttribute("publickey").toString();
				String privatekey= pageContext.getAttribute("privatekey").toString();
				out.println("publicKey="+publickey);
				out.println("<br/>privateKey="+privatekey);
			
				ReCaptcha captcha = ReCaptchaFactory.newSecureReCaptcha(
					"6LfsDvsSAAAAAIpbunMHzJMkw7JZNDWOvNxLG8HB",
					"6LfsDvsSAAAAAC7I6Zi670E2Oji1blOmblyGjcF9", false);
			String captchaScript = captcha.createRecaptchaHtml(
					request.getParameter("error"), null);

			out.print(captchaScript);
		%>
		<input type="submit" value="submit" />
	</form>
</body>
</html>
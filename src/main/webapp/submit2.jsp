
<%@ page import="net.tanesha.recaptcha.ReCaptchaImpl"%>
<%@ page import="net.tanesha.recaptcha.ReCaptchaResponse"%>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha"%>
<%@ page import="java.net.URLEncoder;"%>

<html>
<body>
	<%
		String publickey = "6LfsDvsSAAAAAIpbunMHzJMkw7JZNDWOvNxLG8HB";
		String privatekey = "6LfsDvsSAAAAAC7I6Zi670E2Oji1blOmblyGjcF9";
		// create recaptcha without <noscript> tags
		ReCaptcha captcha = ReCaptchaFactory.newSecureReCaptcha(publickey,
				privatekey, false);
		ReCaptchaResponse cresponse = captcha.checkAnswer(
				request.getRemoteAddr(),
				request.getParameter("recaptcha_challenge_field"),
				request.getParameter("recaptcha_response_field"));

		System.out.println("\nrecaptcha-challenge_field="
				+ request.getParameter("recaptcha_challenge_field"));
		System.out.println("recaptcha-response_field="
				+ request.getParameter("recaptcha_response_field"));
		System.out.println("remote address=" + request.getRemoteAddr());

		System.out.println("privatekey="
				+ URLEncoder.encode(privatekey)
				+ "&remoteip="
				+ URLEncoder.encode(request.getRemoteAddr())
				+ "&challenge="
				+ URLEncoder.encode(request
						.getParameter("recaptcha_challenge_field"))
				+ "&response="
				+ URLEncoder.encode(request
						.getParameter("recaptcha_response_field")));

		if (cresponse.isValid()) {
			out.print("Success");
		} else {
			out.print(cresponse.getErrorMessage());
		}
	%>
</body>
</html>